# Forking Warp Board Game

This is an open source board game I am creating.  I have always wanted to create a board game, and being an open source advocate, wanted to share my game with anyone who wants to enjoy it, help make it better, and contribute to it.

## Get the game

Simply clone this repository.  I've used open source software in every instance to create everything I can about this game. 

You'll need / want the following applications:
- GIMP - For adding / editing any of the .xcf files.  Please make sure to also export any new files / modified files as .png as well.
- Scribus - For editing / adding any of the Gameplay rules.
- LibreOffice Writer - For editing the cards used as part of the game.

### Other items you may want to get:
- Blank Gameboard - You can find these on Amazon, or you can use an old gameboard you no longer want / need.  The game is designed to be 18 inches (458 mm) x 18 inches (458 mm). So finding a gameboard near these measurements is ideal, but not required.
- 1 Six count die (a normal die with numbers 1 through 6)
- 1 Three color die (I made this from a die blank and colored the three colors Red, Yellow, and Green on it with Sharpie pen)
        I put each color on an opposite face to itself, Yellow opposite to Yellow, Red opposite to Red, and Green opposite to Green.
- 2 to 6 Player pieces / tokens (I got some cheap pices from Amazon, but you can use anything you like) - Old game pieces will work fine, bits of cork (as long as you can tell each player piece apart).
- 2.5 inch (64 mm) x 3.5 inches (89 mm) Game card blanks (you can just print them on paper and cut them out if you prefer).

## Attribution
Original Game Board Backgrond Art from:
https://get.pxhere.com/photo/light-abstract-spiral-wave-number-pattern-line-color-flame-blue-circle-neon-background-image-font-lines-background-illustration-digital-graphic-vibrations-rotors-luminosity-strudel-image-editing-eddy-sog-light-flowers-vortex-computer-wallpaper-fractal-art-475940.jpg

## LICENSE
This game is freely without warranty against defect, to anyone who wants to play it, modify it,  or change it in any way. 